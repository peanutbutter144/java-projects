/*
 * Tom Ient
 * A simple program to output a conversion table of inches to metres, up to a yard
 */
class inchToMetreTable {  
	public static void main(String args[]) {
		int inches;
		double metres;
		for(inches = 1; inches <= 3*12; inches++) {
			metres = inches / 39.37;
			System.out.println(inches + " inches is " + metres + " metres."); 
			// every foot, print a blank line
			if((inches % 12) == 0) System.out.println(); 
		}
	}
}

