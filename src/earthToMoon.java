/*
 * Tom Ient
 * This program takes a weight value in kg and outputs what it would be equivalent to on the moon.
 */
public class earthToMoon {
	public static void main(String args[]) {
		// define weight on earth. can be anything you want
		double earthWeight = 50;
		double moonWeight = earthWeight * 0.17; // do the calculation
		System.out.println(earthWeight + "kg on Earth is equivalent to " + moonWeight + "kg on the Moon.");
	}
}
